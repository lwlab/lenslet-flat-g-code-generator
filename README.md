# Lenslet Flat Press: G-Code Generator

This program generates G-codes (instructions for a CNC machine) which are used to cut out one side of a flat epoxy press. This press is used to test the performance of a batch of epoxy, by pressing a layer of epoxy onto an alumiuna billet and examining the coated billet using fourier transform spectroscopy (FTS).

The epoxy must be laid on the press at a precise thickness. The thickness is defined by four standoffs that are located on the edges of the press. Epoxy is poured into the center of the press, and the press is clamped together with the alumina billet and heated in an oven during the curing process. 

This program starts with a 6x4x0.5 inch aluminum block and turns it into a square press element. 

## Parameters

`blank_height`, `blank_width` and `blank_depth` are the blank dimensions. The zero point is the bottom of the blank.

`tool_diameter` is the diameter of the cutting tool. The tool is assumed to be cylindrical, and capable of cutting in a downward moving direction.

`grip_width`, `grip_remains`, and `grip_fraction` define the parameters of the edge grip. The edge grip is clamped to the bed of the CNC. The "remains" is the thickness of the arms that connect the grip to the press itself. The fraction is the fractional height of the block which is not fully removed. The grip width is not actually used.

`standoff_height`, etc. define the dimensions of the standoff. The standoff is the part which touches the alumina billet, and creates the precise thickness layer between the billet and the press.

`max_cut_per_strafe` is the maximum cutting depth for the tool per strafe. If it is smaller, then the program requires many more strafes to make deep cuts.

`max_cut_per_strafe_fine` defines the maximum cut depth for the finest surface finish.

## Usage

Run the program using `main.py`. This writes the output file `output.cnc`, which is the list of G-codes.

## CNCSimulatorPro

In simulator software CNCSimulator Pro, it is possible to define the rough block using the Inventory Browser (F2) to define a block in slot 1, and the following special header code:

 #region
 $Inches (Alarm if not inches set)
 $Mill (Alarm if not a milling machine)
 $AddRegPart 1, 0, 0 ,0 
 ET32 G43 H0 M06 
 #endregion