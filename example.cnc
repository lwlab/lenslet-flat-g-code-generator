#region
$Inches (Alarm if not inches set)
$Mill (Alarm if not a milling machine)
$AddRegPart 1, 0, 0 ,0 
ET32 G43 H0 M06 
#endregi
M05
M09
G93
(initial setup)
G64 G80 G40 G49 G18

(move to first extraction)
G00 Z0.6 X0.5 Y0.5
(begin extraction 1)
M03
G01 X0.5 Y3.5 Z0.5
G01 X0.5 Y0.5 Z0.4
G01 X0.5 Y3.5 Z0.3
G01 X0.5 Y0.5 Z0.2
G01 X0.5 Y3.5 Z0.1
G01 X0.5 Y0.5 Z0.0
G01 X0.5 Y3.5 Z0.0
M05 
G00 Z1
(done with extraction 1)

(move to extraction 2)
G00 Z0.6 X0.5 Y0.5
(begin extraction 2)
M03
G01 X4.5 Y3.5 Z0.5
G01 X4.5 Y0.5 Z0.4
G01 X4.5 Y3.5 Z0.3
G01 X4.5 Y0.5 Z0.2
G01 X4.5 Y3.5 Z0.1
G01 X4.5 Y0.5 Z0.0
G01 X4.5 Y3.5 Z0.0
M05 
(done with extraction 2)

(move to initial plane)
G00 Z1
G00 X0 Y0 Z0.6
(begin initial plane)
M03
G01 X0 Y0 Z0.45
G01 X5 
G01 Y0.3 
G01 X0
G01 Y0.6
G01 X5
G01 Y0.9
G01 X0 
G01 Y1.2
G01 X5
G01 Y1.5
G01 X0 
G01 Y1.8
G01 X5
G01 Y2.1
G01 X0 
G01 Y2.4
G01 X5
G01 Y2.7
G01 X0 
G01 Y3.0
G01 X5
G01 Y3.3
G01 X0 
G01 Y3.6
G01 X5
G01 Y3.9
G01 X0 
G01 Y4.0
M05

(move to fine plane)
G00 Z1
G00 X0.8937 Y0.8937 Z0.44

