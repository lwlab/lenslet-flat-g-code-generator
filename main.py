# Lenslet flat G-code generator
# Author: Colin Merkel
# Based upon Ronald Gagne's work, where he generated G-code
# for a wire grid constructor on the same CNC machine. 

# Tested in CNC Simulator.

from pylab import *
import time
import math

# ---------------------------------
#               Setup 
# (all measurements are in inches.)
# ---------------------------------

blank_height = 0.463
blank_width  = 6
blank_depth  = 4

tool_diameter = 0.375

grip_width		= 0.45
grip_remains	= 0.3
grip_fraction	= 0.25

standoff_height = 0.0130404331 # 160 GHz quarter-wave @ e = 2
standoff_width 	= 0.393701 # 1 cm
standoff_depth 	= 0.393701 # 1 cm

max_cut_per_strafe = 0.02
max_cut_per_strafe_fine = 0.002

clear_through_excess_distance = 0.020 # 20 mil

# The GCodeWriter class formats the output
# so that it has nice line numbers.
class GCodeWriter:
	def __init__(self):
		self.file = open("output.cnc", 'w')
		self.linenumber=1
	def append(self,text,prefix=True):
		if prefix:
			print "%s %s"%(self.prefix(),text)
			self.file.write("%s %s\n"%(self.prefix(),text))
			self.linenumber += 1
		else:
			print text
			self.file.write(text+"\n")
	def prefix(self):
		return "N%d0"%(self.linenumber)

g=GCodeWriter()

# ----------------
# Section 1: Setup
# ----------------

# Starting comments.
g.append( "(Lenslet flat mould cutter, v0.1)", False )
g.append("(DATE: %s)"%time.strftime("%d/%m/%Y"),False)
g.append("(TIME: %s)"%time.strftime("%H:%M:%S"),False)

g.append("(MILL SPEED: 2200 RPM)")
g.append("(FEED RATE : 1.33 FPM)")

# Set the tool sizes, and specify tool length compensation.
g.append("G90 G64 G50 G54 G80 G17 G40 G49")
g.append("G20")
g.append("M998")
g.append("T21 G43 H21 M6")
g.append("ET32 G43 H0 M05")
# Turn coolant off.
g.append("S2020 M3 M8")
# Normal cutting mode. Cancel unused settings.
g.append("G0 G94 X0.6215 Y0.821 Z2.5 A0.")
g.append("M01") # Optional stop
# ------------------------
# Section 2: Extraction #1
# ------------------------

g.append("G00 Z%f" % (blank_height+0.2)) # Move the bit up high enough to be off the surface
# Move to extraction point A.
g.append("G00 X%f Y%f" % (grip_width + tool_diameter/2, grip_remains + tool_diameter/2) ) 
g.append("M03") # Start mill.
g.append("G01 Z%f F2.0" % blank_height) # Lower bit to cutting surface.
# Begin first half-strafe.
close = True
for z in linspace(blank_height, blank_height*grip_fraction, blank_height / max_cut_per_strafe ):
	g.append("G01 Z%f F2.0" % z)
	if close:
		g.append("G01 Y0 F15.0")
		close = False
	else:
		g.append("G01 Y%f F15.0" % blank_depth)
		close = True
# Begin second half-strafe
for z in linspace(grip_fraction*blank_height, 0 - clear_through_excess_distance, blank_height / max_cut_per_strafe ):
	g.append("G01 Z%f F2.0" % z)
	if close:
		g.append("G01 Y%f F15.0" % (blank_depth - grip_remains - tool_diameter/2))
		close = False
	else:
		g.append("G01 Y%f F15.0" % (grip_remains + tool_diameter/2))
		close = True


g.append("M05") # Stop mill.
g.append("G00 Z%f" % (blank_height+2)) # Move bit well above the surface.
g.append("M01") # Optional stop

# ------------------------
# Section 3: Extraction #2
# ------------------------

g.append("G00 Z%f" % (blank_height+0.2)) # Move the bit up high enough to be off the surface
# Move to extraction point B.
g.append("G00 X%f Y%f" % (blank_width - grip_width - tool_diameter/2, grip_remains + tool_diameter/2) ) 
g.append("M03") # Start mill.
g.append("G01 Z%f F2.0" % blank_height) # Lower bit to cutting surface.
# Begin first half-strafe.
close = True
for z in linspace(blank_height, blank_height*grip_fraction, blank_height / max_cut_per_strafe ):
	g.append("G01 Z%f F2.0" % z)
	if close:
		g.append("G01 Y0 F15.0")
		close = False
	else:
		g.append("G01 Y%f F15.0" % blank_depth)
		close = True
# Begin second half-strafe
for z in linspace(grip_fraction*blank_height, 0 - clear_through_excess_distance, blank_height / max_cut_per_strafe ):
	g.append("G01 Z%f F2.0" % z)
	if close:
		g.append("G01 Y%f F15.0" % (blank_depth - grip_remains - tool_diameter/2))
		close = False
	else:
		g.append("G01 Y%f F15.0" % (grip_remains + tool_diameter/2))
		close = True

g.append("M05") # Stop mill.
g.append("G00 Z%f" % (blank_height+2)) # Move bit well above the surface.
g.append("M01") # Optional stop

# ------------------------
# Section 4: Rough plane
# ------------------------

g.append("G00 Z%f" % (blank_height + 1)) # Move the well above the surface.
# Move to plane point C.
g.append("G00 X%f Y0" % (grip_width + tool_diameter/2))
g.append("M03")
g.append("G01 Z%f F2.0" % (blank_height - max_cut_per_strafe)) # Cut into material.
# Begin strafe.
xmin = (grip_width + tool_diameter/2)
xmax = (blank_width - grip_width - tool_diameter/2)
close = True
for x in linspace(xmin, xmax, 2*(xmax-xmin)/tool_diameter):
	g.append("G01 X%f F15.0" % x)
	if close:
		g.append("G01 Y%f F15.0" % blank_depth)
		close = False
	else:
		g.append("G01 Y0 F15.0")
		close = True

# Here, we don't stop the mill or raise the bit above the surface
# in order to preserve the accuracy of the Z value.

# -----------------------------
# Section 5: Fine plane, part 1
# (the central region)
# -----------------------------
g.append("(Fine plane, part 1)", False)

zmin = blank_height - max_cut_per_strafe - max_cut_per_strafe_fine
zmax = blank_height - max_cut_per_strafe - standoff_height + max_cut_per_strafe_fine
for z in linspace(zmin, zmax, abs((zmax-zmin)/(max_cut_per_strafe_fine))):
	# Move to plane point C, while cutting.
	g.append("G01 Y0 F6") # Slowly move bit, while cutting, to position D.
	g.append("G01 X%f F6" % (grip_width + standoff_width + 1.5*tool_diameter))

	# Now we will cut into the material.
	g.append("G01 Z%f F2.0" % (z)) # Cut into material.
	# Begin strafe.
	xmin = (grip_width + standoff_width + 1.5*tool_diameter)
	xmax = (blank_width - standoff_width - grip_width - 1.5*tool_diameter)
	close = True
	for x in linspace(xmin, xmax, 2*(xmax-xmin)/tool_diameter):
		g.append("G01 X%f F15.0" % x)
		if close:
			g.append("G01 Y%f F15.0" % blank_depth)
			close = False
		else:
			g.append("G01 Y0 F15.0")
			close = True

# Now perform the final cut.

# Move to plane point C, while cutting.
g.append("G01 Y0 F6") # Slowly move bit, while cutting, to position D.
g.append("G01 X%f F6" % (grip_width + standoff_width + 1.5*tool_diameter))

# Now we will cut into the material.
g.append("G01 Z%f F2.0" % (blank_height - max_cut_per_strafe - standoff_height)) # Cut into material.
# Begin strafe.
xmin = (grip_width + standoff_width + 1.5*tool_diameter)
xmax = (blank_width - standoff_width - grip_width - 1.5*tool_diameter)
close = True
for x in linspace(xmin, xmax, 4*(xmax-xmin)/tool_diameter):
	g.append("G01 X%f F6.0" % x)
	if close:
		g.append("G01 Y%f F6.0" % blank_depth)
		close = False
	else:
		g.append("G01 Y0 F6.0")
		close = True

# Again: don't stop the bit, and don't raise it.

# -------------------------------
#  Section 6: Fine plane, part 2
# (the zig-zag region, left side)
# -------------------------------
g.append("(Fine plane, part 2)", False)

# Move to plane point C, while cutting.
g.append("G01 Y%f F15" % (standoff_depth + tool_diameter/2)) # Slowly move bit, while cutting, to position E.
g.append("G01 X%f F15" % (grip_width + tool_diameter))

# Begin strafe.
ymin = (tool_diameter/2 + standoff_depth)
ymax = (blank_depth - tool_diameter/2 - standoff_depth)
close = True
for y in linspace(ymin, ymax, 2*(ymax-ymin)/tool_diameter):
	g.append("G01 Y%f F15.0" % y)
	if close:
		g.append("G01 X%f F15.0" % (tool_diameter + grip_width + standoff_width))
		close = False
	else:
		g.append("G01 X%f F15.0" % (tool_diameter/2+grip_width + max_cut_per_strafe))
		close = True

# -------------------------------
#  Section 6: Fine plane, part 2
# (the zig-zag region, right side)
# -------------------------------

# Move to plane point C, while cutting.
g.append("G01 Y%f F15" % (standoff_depth + tool_diameter/2)) # Slowly move bit, while cutting, to position E.
g.append("G01 X%f F15" % (blank_width - grip_width - tool_diameter))

# Begin strafe.
ymin = (tool_diameter/2 + standoff_depth)
ymax = (blank_depth - tool_diameter/2 - standoff_depth)
close = True
for y in linspace(ymin, ymax, 4*(ymax-ymin)/tool_diameter):
	g.append("G01 Y%f F15.0" % y)
	if close:
		g.append("G01 X%f F15.0" % (blank_width - tool_diameter - grip_width - standoff_width))
		close = False
	else:
		g.append("G01 X%f F15.0" % (blank_width - tool_diameter/2 - grip_width - max_cut_per_strafe))
		close = True

g.append("M05") # Stop mill.
g.append("G00 Z%f" % (blank_height+2)) # Raise bit above surface.
g.append("M00") # Compulsory stop